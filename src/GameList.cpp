//
// Created by Gabe Blandin on 12/7/2023.
//

#include "GameList.hpp"
#include "Minesweeper.hpp"
#include <format>

std::optional<GameType> getType(std::string_view input_string) {
    int input{};
    auto end{input_string.data() + input_string.length()};
    if (std::from_chars(input_string.data(), end, input).ptr != end) {
        return {};
    }
    return getType(input);
}

std::optional<GameType> getType(int input) {
    switch (input) {
        case static_cast<int>(GameType::Minesweeper):
            return GameType::Minesweeper;
        default:
            return {};
    }
}

std::string allGames() {
    std::string game_string{};
    for (const auto& [key, value]: GAME_LIST) {
        game_string += std::format("{}. {}\n", static_cast<int>(key), value);
    }
    return game_string;
}

std::unique_ptr<Game> getGame(GameType type) {
    switch (type) {
        case GameType::Minesweeper:
            return std::make_unique<Minesweeper>();
        default:
            return nullptr;
    }
}