//
// Created by Gabe Blandin on 11/20/2023.
//

#ifndef MULTI_GAME_MINESWEEPER_HPP
#define MULTI_GAME_MINESWEEPER_HPP

#include "Game.hpp"
#include <array>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>

struct Difficulty {
    // Starting at one mainly for displaying
    enum Level {
        Easy = 1,
        Medium,
        Hard
    };
    const std::string_view name;
    const int board_rows, board_columns, bomb_count;
};

const std::map<Difficulty::Level, Difficulty> DIFFICULTY_MAP{
        {Difficulty::Easy, {"Easy", 9, 9, 10}},
        {Difficulty::Medium, {"Medium", 16, 16, 40}},
        {Difficulty::Hard, {"Hard", 30, 16, 99}},
};

// Represents one square on the board
// value is the number of bombs around this square where -1 indicates this
// square itself is a bomb.
struct Square {
    enum State {
        Hidden,
        Flagged,
        Revealed
    };
    int value{0};
    State state{Hidden};
};

class Minesweeper : public Game {
    int m_rows{0}, m_columns{0}, m_bomb_count{0};
    std::vector<std::vector<Square>> m_board;

public:
    Minesweeper() = default; // Maybe we could load in saved custom difficulties in the constructor?
    ~Minesweeper() override;

    int setUp() override;
    int start() override;
    [[nodiscard]] std::set<std::pair<int, int>> genBombCoords() const;
    void printBoard();
};

// Non-member helpers
int calcSquareValue(const std::set<std::pair<int, int>>& t_bomb_coords, int t_row, int t_column);

#endif// MULTI_GAME_MINESWEEPER_HPP
