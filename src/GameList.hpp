//
// Created by Gabe Blandin on 12/7/2023.
//

#ifndef MULTI_GAME_GAMELIST_HPP
#define MULTI_GAME_GAMELIST_HPP

#include "Game.hpp"
#include <map>
#include <memory>
#include <optional>
#include <string_view>

// Starting at one mainly for displaying
enum class GameType {
    Minesweeper = 1,
};

const std::map<GameType, std::string_view> GAME_LIST{
        {GameType::Minesweeper, "Minesweeper"},
};

std::optional<GameType> getType(std::string_view input_string);
std::optional<GameType> getType(int input);
std::string allGames();
std::unique_ptr<Game> getGame(GameType type);

#endif// MULTI_GAME_GAMELIST_HPP
