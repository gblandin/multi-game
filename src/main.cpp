#include <iostream>
#include <format>
#include "Game.hpp"
#include "GameList.hpp"

int main() {
    std::cout << "Welcome to Multi-Game!\n";

    while (true) {
        std::cout << "Please select a game from the list below by typing in the number or type 'q' to quit.\n";
        std::cout << allGames();
        std::string input{};
        std::getline(std::cin >> std::ws, input);

        if (input == "q") {
            std::cout << "Thanks for playing!";
            break;
        }

        auto game_type{getType(input)};
        if (!game_type) {
            std::cout << "Invalid input.\n";
            continue;
        }

        auto game = getGame(*game_type);
        if (game == nullptr) {
            std::cout << "Unknown game.\n"; // Shouldn't happen, but just in case
            continue;
        }
        // TODO handle returned code from these
        game->setUp();
        game->start();
    }

    return 0;
}

