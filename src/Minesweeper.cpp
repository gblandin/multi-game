//
// Created by Gabe Blandin on 11/20/2023.
//

#include "Minesweeper.hpp"
#include <iostream>
#include <random>
#include <set>

Minesweeper::~Minesweeper() {
    std::cout << "Thanks for playing Minesweeper!\n";
    for (auto &row: m_board) {
        row.clear();
    }
    m_board.clear();
    m_board.shrink_to_fit();
}

int Minesweeper::setUp() {
    // TODO: Get difficulty from user input here
    //  1. Print difficulties from map
    //  2. Get input
    //  3. Validate input/Get difficulty enum
    //    a. If user inputs 'q', exit
    //  4. Change line below to use retrieved enum
    Difficulty difficulty = DIFFICULTY_MAP.at(Difficulty::Easy);
    m_rows = difficulty.board_rows;
    m_columns = difficulty.board_columns;
    m_bomb_count = difficulty.bomb_count;

    std::set<std::pair<int, int>> bomb_coords = genBombCoords();

    // Initialize the board
    for (int i = 0; i < m_rows; i++) {
        std::vector<Square> row;
        for (int j = 0; j < m_columns; j++) {
            Square s{calcSquareValue(bomb_coords, i, j)};
            row.push_back(s);
        }
        m_board.push_back(row);
    }
    return 0;
}

std::set<std::pair<int, int>> Minesweeper::genBombCoords() const {
    std::set<std::pair<int, int>> bomb_coords;
    std::random_device rd;
    std::default_random_engine engine(rd());
    std::uniform_int_distribution<int> row_distribution(0, m_rows - 1);
    std::uniform_int_distribution<int> col_distribution(0, m_columns - 1);

    // Randomly generate unique coordinates
    while (static_cast<int>(bomb_coords.size()) < m_bomb_count) {
        int row = row_distribution(engine);
        int col = col_distribution(engine);
        std::pair<int, int> coord{row, col};

        if (bomb_coords.find(coord) == bomb_coords.end()) {
            bomb_coords.insert(coord);
        }
    }

    return bomb_coords;
}

int calcSquareValue(const std::set<std::pair<int, int>>& t_bomb_coords, int t_row, int t_column) {
    int value = 0;
    if (t_bomb_coords.find({t_row, t_column}) != t_bomb_coords.end()) {
        value = -1;
    } else {
        // Check if any surrounding coordinates are bombs and add that to value
        for (int row = -1; row <= 1; row++) {
            for (int col = -1; col <= 1; col++) {
                // Skip this square (row and col == 0) then check if the neighbor square is a bomb and add to value if so
                if (!(row == 0 && col == 0) &&
                    t_bomb_coords.find({t_row + row, t_column + col}) != t_bomb_coords.end()) {
                    value++;
                }
            }
        }
    }
    return value;
}

int Minesweeper::start() {
    std::cout << "Minesweeper!\n";
    printBoard();
    // TODO:
    //  1. Get user input (x y <f for flagging; optional>)
    //  2. Validate input
    //    a. If flagged (and not unflagging), invalid
    //    b. If out of bounds, invalid
    //  3. Perform action (flag or reveal)
    //    a. If bomb, print board, fail, and exit game (showing all bombs on fail will come later)
    //    b. If value == 0, reveal all neighbor 0 up to first non-0
    //    c. If flagging or unflagging, just change state
    //    d. If all non-bombs revealed, win, print board, and exit game

    return 0;
}

void Minesweeper::printBoard() {
    // TODO: Change this to print display values
    //  1. Print row of just numbers 1-m_columns
    //  2. Print row number before iterating through row columns
    //  3. Print square with logic
    //    a. If hidden, print '*' or '_' or '-' or similar (maybe unicode square if possible?)
    //    b. If flagged, print 'F' (or flag emoji or similar if possible)
    //    c. If revealed, print ' ' for 0, 'B' (or bomb emoji if possible) for -1, or numeric value otherwise
    for (const auto &row: m_board) {
        for (const auto &square: row) {
            std::cout << (square.value == -1 ? "" : " ") << square.value << " ";
        }
        std::cout << "\n";
    }
}
