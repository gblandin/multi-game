//
// Created by Gabe Blandin on 11/20/2023.
//

#ifndef MULTI_GAME_GAME_HPP
#define MULTI_GAME_GAME_HPP

class Game {
protected:
    // If needed, initialize anything that will be needed regardless of user options
    Game() = default;

public:
    // Ensure to perform any cleanup that isn't automatic
    virtual ~Game() = default;
    // This should be where users set options and any initialization based on options happens
    virtual int setUp() = 0;
    // This should be where the main gameplay loop lives
    virtual int start() = 0;
};

#endif// MULTI_GAME_GAME_HPP