# multi-game

## Description
multi-game is a simple project focused on utilizing some of the basics of C++ 
to implement a simple minesweeper game and a deck of cards 
(possibly some card game) within the same application. May also expand to 
contain some other simple games.

## Installation
TBD

## Support
If you have any suggestions for improvement, please drop an issue and I will 
look it over at the next opportunity.

## Contributing
This is a small personal/learning project so no contributions will be 
accepted at this time.

## Current TODOs
* Work on adding the minesweeper implementation
* Work on implementing a full deck of cards
* Eventually add GUI
* Add .gitlab-ci.yml
